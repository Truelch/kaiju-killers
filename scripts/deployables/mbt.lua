--inspired from tosx
--[[
local this = {}


function this:init(mod)
	local this = self
	modApi:appendAsset("img/units/player/smalltank_1.png",        mod.resourcePath .."img/units/deployables/smalltank_1.png")
	modApi:appendAsset("img/units/player/smalltank_1a.png",       mod.resourcePath .."img/units/deployables/smalltank_1a.png")
	modApi:appendAsset("img/units/player/smalltank_1_death.png",  mod.resourcePath .."img/units/deployables/smalltank_1_death.png")
	modApi:appendAsset("img/units/player/smalltank_1_ns.png",     mod.resourcePath .."img/units/deployables/smalltank_1_ns.png")
	
	setfenv(1, ANIMS)	
	truelch_MBT_img =    MechUnit:new{ Image = "units/player/tosx_carrier_drone.png", PosX = -15, PosY = 0 }
	truelch_MBT_imga =	 tosx_Fighter_img:new{ Image = "units/player/tosx_carrier_dronea.png", NumFrames = 4 }
	truelch_MBT_img_ns = MechIcon:new{ Image = "units/player/tosx_carrier_dronens.png" }
	truelch_MBT_imgd =   tosx_Fighter_img:new{ Image = "units/player/tosx_carrier_droned.png", Loop = false, PosX = -17, Time = 0.075, NumFrames = 11 }
end

return this
]]

--inspired from template
-- this line just gets the file path for your mod, so you can find all your files easily.
local path = mod_loader.mods[modApi.currentMod].resourcePath

-- locate our mech assets.
local deployablePath = path .."img/units/deployables/"

-- make a list of our files.
local files = {
	"smalltank_1.png",
	"smalltank_1a.png",
	"smalltank_1_ns.png",
	"smalltank_1_death.png",
}

-- iterate our files and add the assets so the game can find them.
for _, file in ipairs(files) do
	modApi:appendAsset("img/units/player/" .. file, deployablePath .. file)
end

-- create animations for our mech with our imported files.
-- note how the animations starts searching from /img/
local a = ANIMS
a.mbt    = a.MechUnit:new{Image = "units/player/smalltank_1.png",  PosX = -15, PosY = 10 }
a.mbta   = a.MechUnit:new{Image = "units/player/smalltank_1a.png", PosX = -15, PosY = 10, NumFrames = 2 }
a.mbt_ns = a.MechIcon:new{Image = "units/player/smalltank_1_ns.png" }
a.mbtd   = a.MechIcon:new{Image = "units/player/smalltank_1_death.png", Loop = false,  PosX = -15, PosY = 1, Time = 0.075, NumFrames = 11 }