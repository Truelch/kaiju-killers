--inspired from template
-- this line just gets the file path for your mod, so you can find all your files easily.
local path = mod_loader.mods[modApi.currentMod].resourcePath

-- locate our mech assets.
local deployablePath = path .."img/units/deployables/"

-- make a list of our files.
local files = {
	"flametank.png",
	"flametanka.png",
	"flametank_ns.png",
	"flametank_death.png",
}

-- iterate our files and add the assets so the game can find them.
for _, file in ipairs(files) do
	modApi:appendAsset("img/units/player/" .. file, deployablePath .. file)
end

-- create animations for our mech with our imported files.
-- note how the animations starts searching from /img/
local a = ANIMS
a.pyrotank    = a.MechUnit:new{Image = "units/player/flametank.png",  PosX = -12, PosY = 5 }
a.pyrotanka   = a.MechUnit:new{Image = "units/player/flametanka.png", PosX = -12, PosY = 5, NumFrames = 2 }
a.pyrotank_ns = a.MechIcon:new{Image = "units/player/flametank_ns.png" }
a.pyrotankd   = a.MechIcon:new{Image = "units/player/flametank_death.png", Loop = false,  PosX = -15, PosY = -4, Time = 0.075, NumFrames = 11 }