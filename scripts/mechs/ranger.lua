
-- this line just gets the file path for your mod, so you can find all your files easily.
local path = mod_loader.mods[modApi.currentMod].resourcePath

-- locate our mech assets.
local mechPath = path .."img/units/mechs/"

-- make a list of our files.
local files = {
	"ranger.png",
	"ranger_a.png",
	"ranger_w.png",
	"ranger_w_broken.png",
	"ranger_broken.png",
	"ranger_ns.png",
	"ranger_h.png"
}

-- iterate our files and add the assets so the game can find them.
for _, file in ipairs(files) do
	modApi:appendAsset("img/units/player/" .. file, mechPath .. file)
end

-- create animations for our mech with our imported files.
-- note how the animations starts searching from /img/
local a = ANIMS
a.ranger =			a.MechUnit:new{Image = "units/player/ranger.png",          PosX = -20, PosY = -3}
a.rangera =			a.MechUnit:new{Image = "units/player/ranger_a.png",        PosX = -21, PosY = -3, NumFrames = 4 }
a.rangerw =			a.MechUnit:new{Image = "units/player/ranger_w.png",        PosX = -19, PosY = 6 }
a.ranger_broken =	a.MechUnit:new{Image = "units/player/ranger_broken.png",   PosX = -16, PosY = 2 }
a.rangerw_broken =	a.MechUnit:new{Image = "units/player/ranger_w_broken.png", PosX = -17, PosY = 8 }
a.ranger_ns =		a.MechIcon:new{Image = "units/player/ranger_ns.png"}

RangerMech = Pawn:new{
	Name = "Ranger Mech",
	Class = "Brute",
	
	Health = 2,
	MoveSpeed = 5,
	Massive = true,
	
	Image = "ranger",
	ImageOffset = 4,
	
	-- Any weapons this mech should start with goes in this table.
	SkillList = { "truelch_kk_FatMac" },
	--SkillList = { "lmn_Guided_Missile", "lmn_Multi_Laser" },

	SoundLocation = "/mech/prime/punch_mech/", -- movement sounds.
	ImpactMaterial = IMPACT_METAL, -- impact sounds.
	
	DefaultTeam = TEAM_PLAYER,
}