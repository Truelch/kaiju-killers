
-- this line just gets the file path for your mod, so you can find all your files easily.
local path = mod_loader.mods[modApi.currentMod].resourcePath

-- locate our mech assets.
local mechPath = path .."img/units/mechs/"

-- make a list of our files.
local files = {
	"demo.png",
	"demo_a.png",
	"demo_w.png",
	"demo_w_broken.png",
	"demo_broken.png",
	"demo_ns.png",
	"demo_h.png"
}

-- iterate our files and add the assets so the game can find them.
for _, file in ipairs(files) do
	modApi:appendAsset("img/units/player/" .. file, mechPath .. file)
end

-- create animations for our mech with our imported files.
-- note how the animations starts searching from /img/
local a = ANIMS
a.demo =			a.MechUnit:new{Image = "units/player/demo.png",          PosX = -20, PosY = -3}
a.demoa =			a.MechUnit:new{Image = "units/player/demo_a.png",        PosX = -21, PosY = -3, NumFrames = 4 }
a.demow =			a.MechUnit:new{Image = "units/player/demo_w.png",        PosX = -19, PosY = 6 }
a.demo_broken =	    a.MechUnit:new{Image = "units/player/demo_broken.png",   PosX = -16, PosY = 2 }
a.demow_broken =	a.MechUnit:new{Image = "units/player/demo_w_broken.png", PosX = -17, PosY = 8 }
a.demo_ns =		    a.MechIcon:new{Image = "units/player/demo_ns.png"}

DemoMech = Pawn:new{
	Name = "Demo Mech",
	Class = "Ranged",
	
	Health = 2,
	MoveSpeed = 2,
	Massive = true,
	
	Image = "demo",
	ImageOffset = 4,
	
	SkillList = { "truelch_kk_NukeLauncher" },
	
	SoundLocation = "/mech/prime/punch_mech/", -- movement sounds.	
	ImpactMaterial = IMPACT_METAL, -- impact sounds.
	
	DefaultTeam = TEAM_PLAYER,
}
