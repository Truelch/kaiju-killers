
-- this line just gets the file path for your mod, so you can find all your files easily.
local path = mod_loader.mods[modApi.currentMod].resourcePath

-- locate our mech assets.
local mechPath = path .."img/units/mechs/"

-- make a list of our files.
local files = {
	"bootcamp.png",
	"bootcamp_a.png",
	"bootcamp_w.png",
	"bootcamp_w_broken.png",
	"bootcamp_broken.png",
	"bootcamp_ns.png",
	"bootcamp_h.png"
}

-- iterate our files and add the assets so the game can find them.
for _, file in ipairs(files) do
	modApi:appendAsset("img/units/player/" .. file, mechPath .. file)
end

-- create animations for our mech with our imported files.
-- note how the animations starts searching from /img/
local a = ANIMS
a.bootcamp =			a.MechUnit:new{Image = "units/player/bootcamp.png",          PosX = -20, PosY = -3}
a.bootcampa =			a.MechUnit:new{Image = "units/player/bootcamp_a.png",        PosX = -21, PosY = -3, NumFrames = 4 }
a.bootcampw =			a.MechUnit:new{Image = "units/player/bootcamp_w.png",        PosX = -19, PosY = 6 }
a.bootcamp_broken =	    a.MechUnit:new{Image = "units/player/bootcamp_broken.png",   PosX = -16, PosY = 2 }
a.bootcampw_broken =	a.MechUnit:new{Image = "units/player/bootcamp_w_broken.png", PosX = -17, PosY = 8 }
a.bootcamp_ns =		    a.MechIcon:new{Image = "units/player/bootcamp_ns.png"}


-- we can make a mech based on another mech much like we did with weapons.
BootcampMech = Pawn:new{
	Name = "Bootcamp Mech",
	Class = "Science",

	Health = 3,
	MoveSpeed = 3,
	Massive = true,
	
	Image = "bootcamp",
	ImageOffset = 4,
	
	SkillList = { "truelch_kk_Quartermaster" },	
	--SkillList = { "truelch_kk_DeployTank" },

	SoundLocation = "/mech/prime/punch_mech/", -- movement sounds.
	ImpactMaterial = IMPACT_METAL, -- impact sounds.
	
	DefaultTeam = TEAM_PLAYER,
}