
-- init.lua is the entry point of every mod

local mod = {
	id = "truelch_KaijuKillers",
	name = "Kaiju Killers",
	version = "0.0.3",
	requirements = {},
	modApiVersion = "2.3.0",
	icon = "img/mod_icon.png"
}

function mod:init()
	--Libs
	require(self.scriptPath .. "LApi/LApi")
	require(self.scriptPath .. "libs/artilleryArc")


	--Mechs
	require(self.scriptPath .. "mechs/ranger")
	require(self.scriptPath .. "mechs/demo")
	require(self.scriptPath .. "mechs/bootcamp")

	--Deployables
	mbt = require(self.scriptPath .. "deployables/mbt")
	pyrotank = require(self.scriptPath .. "deployables/pyrotank")

	--Weapons
	require(self.scriptPath .. "weapons/fat_mac")
	require(self.scriptPath .. "weapons/nuke_launcher")
	--require(self.scriptPath .. "weapons/quartermaster")
	require(self.scriptPath .. "weapons/deploy_tank")

	--Effects
	modApi:appendAsset("img/effects/shotup_mbt.png",      self.resourcePath .. "img/effects/shotup_mbt.png")
	modApi:appendAsset("img/effects/shotup_pyrotank.png", self.resourcePath .. "img/effects/shotup_pyrotank.png")


end

function mod:load(options, version)
	-- after we have added our mechs, we can add a squad using them.
	modApi:addSquad(
		{
			"Kaiju Killers",		-- title
			"RangerMech",			-- mech #1
			"DemoMech",				-- mech #2
			"BootcampMech"			-- mech #3
		},
		"Kaiju Killers",
		"Idea from mischiefdemonica, created by Truelch using Lemonymous template.",
		self.resourcePath .."img/squad_icon.png"
	)
end

return mod