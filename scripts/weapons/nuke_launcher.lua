-- this line just gets the file path for your mod, so you can find all your files easily.
local path = mod_loader.mods[modApi.currentMod].resourcePath

-- add assets from our mod so the game can find them.
modApi:appendAsset("img/weapons/truelch_kk_NukeLauncher.png", path .."img/weapons/truelch_kk_NukeLauncher.png")

truelch_kk_NukeLauncher = Skill:new{
	--Infos
	Name = "Nuclear RPG-7-US",
	Description = "<TODO>",
	Icon = "weapons/truelch_kk_NukeLauncher.png", -- notice how the game starts looking in /img/
	Class = "Ranged",
	PowerCost = 2,
	--From vanilla arty
	Range = RANGE_ARTILLERY,
	UpShot = "effects/shot_artimech.png",
	ArtilleryStart = 1,
	ArtillerySize = 8,
	BounceAmount = 3,
	--BuildingDamage = true,
	Push = 1,
	DamageOuter = 0,
	DamageCenter = 1, --DamageCenter = 2,
	Damage = 1, --USED FOR TOOLTIPS
	Explosion = "",
	ExplosionCenter = "ExploArt1",
	ExplosionOuter = "",
	OuterAnimation = "airpush_",
	--Upgrades
	Upgrades = 2,
	UpgradeList = { "+1 Center Damage", "+1 Damage" },
	UpgradeCost = { 1, 3 },
	--Custom
	NegatePsion = 1, --TODO: progressive area of effect
	--Tip image
	TipImage = {
		Unit = Point(2,3),
		Enemy = Point(2,2),
		Enemy2 = Point(3,2),
		Enemy3 = Point(2,1),
		Target = Point(2,2),
	},
}

truelch_kk_NukeLauncher_A = truelch_kk_NukeLauncher:new{
	UpgradeDescription = "Increases the damage dealt to the center by 1.",
	DamageCenter = 2, --Real damage
	Damage = 2, --Tip
}

truelch_kk_NukeLauncher_B = truelch_kk_NukeLauncher:new{
	UpgradeDescription = "Increases all the damage by 1.",
	DamageCenter = 2, --Real damage
	DamageOuter = 1,
	Damage = 2, --Tip
}

truelch_kk_NukeLauncher_AB = truelch_kk_NukeLauncher:new{
	DamageCenter = 3, --Real damage
	DamageOuter = 1,
	Damage = 3, --Tip
}

function truelch_kk_NukeLauncher:GetTargetArea(p1)
	local ret = PointList()	
	for dir = DIR_START, DIR_END do
		for i = 1, 8 do
			local curr = Point(p1 + DIR_VECTORS[dir] * i)
			if not Board:IsValid(curr) then
				break
			end
			
			if not self.OnlyEmpty or not Board:IsBlocked(curr, PATH_GROUND) then
				ret:push_back(curr)
			end
		end

	end	
	return ret
end

function truelch_kk_NukeLauncher:GetSkillEffect(p1, p2)
	local ret = SkillEffect()
	
	local damage = SpaceDamage(p2, self.DamageCenter)
	damage.sAnimation = self.ExplosionCenter
	
	-- Negate --->
	local targetPawn = Board:GetPawn(p2)
	if self.NegatePsion == 1 and targetPawn ~= nil and targetPawn:GetTeam() == TEAM_ENEMY then
		damage.sScript = string.format([[
		        local pawn = Board:GetPawn(%s)
		        pawn:SetTeam(TEAM_BOTS)
		        pawn:SetMutation(0)
		    ]],
		    p2:GetString()
		)     
	end
	-- <--- Negate
	
	ret:AddBounce(p1, 1)
	ret:AddArtillery(damage, self.UpShot)
	
	if self.BounceAmount ~= 0 then ret:AddBounce(p2, self.BounceAmount) end
	
	for dir = 0, 3 do
		damage = SpaceDamage(p2 + DIR_VECTORS[dir], self.DamageOuter)
		
		if self.Push == 1 then
			damage.iPush = dir
		end
		damage.sAnimation = self.OuterAnimation..dir
		
		if not self.BuildingDamage and Board:IsBuilding(p2 + DIR_VECTORS[dir]) then	
			damage.iDamage = 0
			damage.sAnimation = "airpush_"..dir
		end
		
		ret:AddDamage(damage)
	end

	return ret
end