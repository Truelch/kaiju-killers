--,̶'̶,̶|̶'̶,̶'̶_̶  (loss) version

-- this line just gets the file path for your mod, so you can find all your files easily.
local path = mod_loader.mods[modApi.currentMod].resourcePath

-- add assets from our mod so the game can find them.
modApi:appendAsset("img/weapons/truelch_kk_Quartermaster.png", path .."img/weapons/truelch_kk_Quartermaster.png")

truelch_kk_Quartermaster = Skill:new{
	Name = "Quartermaster",
	Description = "Deploys a MBT at range 1 or a Pyro Tank at range 2 and more.",
	Icon = "weapons/truelch_kk_Quartermaster.png", -- notice how the game starts looking in /img/

	--Upgrades
	Upgrades = 2,
	UpgradeList = { "+1 Move for Tanks", "+1 Use" },
	UpgradeCost = { 1, 2 },

	--Custom
	Limited = 1, --loss version!
	ArtillerySize = 8,
	OnlyEmpty = 1, --should be in deployable, but who knows...

	Projectile1 = "effects/shotup_mbt.png",
	Projectile2 = "effects/shotup_pyrotank.png",

	Pawn1 = "truelch_MBT_loss",
	Pawn2 = "truelch_PyroTank_loss",

	TipImage = {
		Unit = Point(1,3),
		Target = Point(1,1),
		Enemy = Point(2,1),
		Enemy2 = Point(3,2),
		Second_Origin = Point(1,3), --Second_Origin = Point(1,1),
		Second_Target = Point(1,2),
	},
}

truelch_kk_Quartermaster_A = truelch_kk_Quartermaster:new{
	UpgradeDescription = "Increases tanks's move by 1.",
	Pawn1 = "truelch_MBT_loss_A",
	Pawn2 = "truelch_PyroTank_loss_A",
}

truelch_kk_Quartermaster_B = truelch_kk_Quartermaster:new{
	UpgradeDescription = "Allows to deploy 2 tanks during a mission.",
	Limited = 2,
}

truelch_kk_Quartermaster_AB = truelch_kk_Quartermaster:new{
	Pawn1 = "truelch_MBT_loss_A",
	Pawn2 = "truelch_PyroTank_loss_A",
	Limited = 2,
}

function truelch_kk_Quartermaster:GetTargetArea(p1)
	local ret = PointList()
	
	for dir = DIR_START, DIR_END do
		for i = 1, 8 do
			local curr = Point(p1 + DIR_VECTORS[dir] * i)
			if not Board:IsValid(curr) then
				break
			end
			
			if not self.OnlyEmpty or not Board:IsBlocked(curr, PATH_GROUND) then
				ret:push_back(curr)
			end
		end
	end
	
	return ret
end

--Is it at rage 1?
function truelch_kk_Quartermaster:IsPointBlank(p1, p2)
	local dir = GetDirection(p2 - p1)
	local pointBlank = p1 + DIR_VECTORS[dir]
	local isPointBlank = pointBlank == p2
	return isPointBlank
end

function truelch_kk_Quartermaster:GetSkillEffect(p1, p2)
	local ret = SkillEffect()
	local isPointBlank = truelch_kk_Quartermaster:IsPointBlank(p1, p2)
	if isPointBlank then
		local damage = SpaceDamage(p2, 0)
		damage.sPawn = self.Pawn1
		ret:AddArtillery(damage, self.Projectile1)
	else
		local damage = SpaceDamage(p2, 0)
		damage.sPawn = self.Pawn2
		ret:AddArtillery(damage, self.Projectile2)
	end

	return ret
end

truelch_MBT_loss = Deploy_Tank:new{
	Name = "MBT",
	Image = "mbt",
	Health = 1,
	MoveSpeed = 2,
	SkillList = { "truelch_MBT_loss_Cannon" },
}

truelch_MBT_loss_A = truelch_MBT_loss:new{
	MoveSpeed = 3,
}

truelch_MBT_loss_Cannon = TankDefault:new {
	Name = "Cannon",
	Description = "Fires a projectile that pushes and deals 1 damage.",
	Class = "",
	Damage = 1,
	Icon = "weapons/deploy_tank.png",
	Explosion = "",
	Sound = "/general/combat/explode_small",
	Damage = 1,
	Push = 1,
	LaunchSound = "/weapons/modified_cannons",
	ImpactSound = "/impact/generic/explosion",
	TipImage = StandardTips.Ranged,
	ZoneTargeting = ZONE_DIR,

	TipImage = {
		Unit = Point(2,3),
		Enemy = Point(2,1),
		Target = Point(2,1),
		CustomPawn = "truelch_MBT"
	}
}

truelch_PyroTank_loss = Deploy_Tank:new{
	Name = "Pyro Tank",
	Image = "pyrotank",
	Health = 1,
	MoveSpeed = 3,
	SkillList = { "truelch_PyroTank_loss_Flamethrower" },
}

truelch_PyroTank_loss_A = truelch_PyroTank_loss:new{
	MoveSpeed = 4,
}

truelch_PyroTank_loss_Flamethrower = Skill:new{
	Name = "Flamethrower",
	Description = "Ignites and pushes a target in contact.",
	Class = "",
	Icon = "weapons/prime_flamethrower.png",
	Explosion = "",
	Range = 2, -- Tooltip?
	PathSize = 2,
	Damage = 0,
	Push = 1, --Mostly for tooltip, but you could turn it off for some unknown reason
	PowerCost = 0,
	LaunchSound = "/weapons/flamethrower",
	TipImage = {
		Unit = Point(2,3),
		Enemy = Point(2,2),
		Target = Point(2,2),
		CustomPawn = "truelch_PyroTank"
	},
}

function truelch_PyroTank_loss_Flamethrower:GetTargetArea(point)
	local ret = PointList()
	for i = DIR_START, DIR_END do
		for k = 1, self.PathSize do
			local curr = DIR_VECTORS[i]*k + point
			ret:push_back(curr)
			if not Board:IsValid(curr) or Board:GetTerrain(curr) == TERRAIN_MOUNTAIN then
				break
			end
		end
	end
	
	return ret
end
				
function truelch_PyroTank_loss_Flamethrower:GetSkillEffect(p1, p2)
	local ret = SkillEffect()
	local direction = GetDirection(p2 - p1)
	local distance = p1:Manhattan(p2)

	for i = 1, distance do
		local curr = p1 + DIR_VECTORS[direction]*i
		local push = (i == distance) and direction*self.Push or DIR_NONE
		local damage = SpaceDamage(p1 + DIR_VECTORS[direction]*i,0, push)
		damage.iFire = EFFECT_CREATE
		if i == distance then 	
			damage.sAnimation = "flamethrower"..distance.."_"..direction 
		end
		ret:AddDamage(damage)
	end

	return ret
end