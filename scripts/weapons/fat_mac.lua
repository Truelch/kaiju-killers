-- this line just gets the file path for your mod, so you can find all your files easily.
local path = mod_loader.mods[modApi.currentMod].resourcePath

---------------------------

local mod = mod_loader.mods[modApi.currentMod]
local modApiExt = LApi.library:fetch("modApiExt/modApiExt", nil, "ITB-ModUtils") --Oh it worked apparently
--LOG("TRUELCH - modApiExt: " .. tostring("modApiExt"))
local scriptPath = mod.scriptPath
local utils = require(scriptPath .."libs/utils")

---------------------------

-- add assets from our mod so the game can find them.
modApi:appendAsset("img/weapons/truelch_kk_FatMac.png", path .."img/weapons/truelch_kk_FatMac.png")

local function isGame()
	return true
		and Game ~= nil
		and GAME ~= nil
end

local function isMission()
	--LOG("TRUELCH --------------------------------------------- isMission() - A")
	local mission = GetCurrentMission()

	--LOG("TRUELCH --------------------------------------------- isMission() - B")

	return true
		and isGame()
		and mission ~= nil
		and mission ~= Mission_Test
end

local function missionData()
	local mission = GetCurrentMission()

	if mission.truelch_ww2 == nil then
		mission.truelch_ww2 = {}
	end

	if mission.truelch_ww2.achievementData == nil then
		mission.truelch_ww2.achievementData = {}
	end

	return mission.truelch_ww2.achievementData
end

truelch_kk_FatMac = Skill:new{
	Name = "Fat Mac",
	Description = "A non-lethal projectile that phases through obstacles and pushes the first unit in range.\nCan go through any obstacle when both upgraded.",	
	Class = "Brute",
	Icon = "weapons/truelch_kk_FatMac.png", -- notice how the game starts looking in /img/
	Damage = 0,
	--Upgrades
	Upgrades = 2,
	UpgradeList = { "Double Strike", "Fire" },
	UpgradeCost = { 2, 2 },
	--Sound
	LaunchSound = "/weapons/burst_beam",
	--Art
	UpShot = "effects/shot_artimech.png",
	--Switch
	PhasePawn = false,
	PhaseObstacle = false,
	--Custom
	Range = 4,
	DoubleStrike = false,
	Fire = 0,
	--Custom height
	ArtilleryHeight = 0,
	--Tip image
	TipImage = {
		Unit = Point(2,3),
		Enemy = Point(2,1),
		Target = Point(2,1),
		--Mountain = Point(2,2),
	},
}


truelch_kk_FatMac_A = truelch_kk_FatMac:new{
	UpgradeDescription = "Allows to shoot again.\nCan go through any obstacle when both upgraded.",
	DoubleStrike = true,
}

truelch_kk_FatMac_B = truelch_kk_FatMac:new{
	UpgradeDescription = "The projectile sets the target on fire.\nCan go through any obstacle when both upgraded.",
	Fire = 1,
}

truelch_kk_FatMac_AB = truelch_kk_FatMac:new{
	DoubleStrike = true,
	Fire = 1,
	--Both upgraded
	PhasePawn = true,
	PhaseObstacle = true,
		TipImage = {
		Unit = Point(2,3),
		Enemy = Point(2,1),
		Target = Point(2,1),
		Mountain = Point(2,2),
	},
}

function truelch_kk_FatMac:GetTargetArea(p1)
	local ret = PointList()
	for dir = DIR_START, DIR_END do
		for i = 1, self.Range do
			local curr = Point(p1 + DIR_VECTORS[dir] * i)

			if not Board:IsValid(curr) then -- out of the map
				break
			end

			local pawn = Board:GetPawn(curr)

			if pawn ~= nil then
				ret:push_back(curr)
				if self.PhasePawn == false then
					break
				end
			elseif Board:IsBlocked(curr, PATH_PROJECTILE) and not self.PhaseObstacle then
				--test
				break
			end
		end
	end
	return ret
end

function truelch_kk_FatMac:GetSkillEffect(p1, p2)
	local ret = SkillEffect()
	local direction = GetDirection(p2 - p1)
	
	local damage = SpaceDamage(p2, self.Damage)
	damage.iPush = direction
	damage.iFire = self.Fire
	damage.sAnimation = "explopush2_"..direction
	
	ret:AddBounce(p1, 1)
	ret:AddArtillery(damage, self.UpShot)

	local pawn = Board:GetPawn(p1)

	if pawn == nil or isMission() == false then
		--Should not happen, but...
		LOG("TRUELCH --------------------------------------------- No pawn for fat mac reload! or is not a mission...")
		return ret
	end

	local pawnId = pawn:GetId()

	LOG("TRUELCH --------------------------------------------- pawnId: " .. tostring(pawnId))

	if self.DoubleStrike and missionData().fatMacHasShot[pawnId + 1] == false then
		ret:AddScript([[
		    local self = Point(]].. p1:GetString() .. [[)
		    Board:GetPawn(self):SetActive(true)
		    Board:Ping(self, GL_Color(255, 255, 255));
		]])
		missionData().fatMacHasShot[pawnId + 1] = true
	end

	LOG("TRUELCH --------------------------------------------- H")

	return ret
end


--- HOOK ---

local EVENT_TURN_START = 14

modApi.events.onMissionUpdate:subscribe(function()	
	local exit = false
		or isMission() == false

	if exit then
		return
	end

	--LOG("TRUELCH --------------------------------------------- onMissionUpdate")

	if Game:GetEventCount(EVENT_TURN_START) > 0 then
		LOG("TRUELCH --------------------------------------------- TURN START (FAT MAC)")
		missionData().fatMacHasShot = {}
		for i = 1, 3 do
			LOG("TRUELCH --------------------------------------------- i: " .. tostring(i))
			missionData().fatMacHasShot[i] = false
		end
	end
end)

--Debug
Debug_CoresChest = Skill:new{
	Class = "",
	Rarity = 0, --!!!
	Icon = "weapons/debug_coreschest.png",
	PowerCost = 4,
}
