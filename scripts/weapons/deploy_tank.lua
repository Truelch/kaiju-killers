-- this line just gets the file path for your mod, so you can find all your files easily.
local path = mod_loader.mods[modApi.currentMod].resourcePath

-- add assets from our mod so the game can find them.
modApi:appendAsset("img/weapons/truelch_kk_Quartermaster.png", path .."img/weapons/truelch_kk_Quartermaster.png")

truelch_kk_DeployTank = Skill:new{
	Name = "Deploy Tank",
	Description = "Deploys a MBT.",
	Icon = "weapons/truelch_kk_Quartermaster.png", -- notice how the game starts looking in /img/

	--Upgrades
	Upgrades = 2,
	UpgradeList = { "+1 Damage", "+2 HP" },
	UpgradeCost = { 2, 2 },

	--Custom
	ArtillerySize = 8,
	OnlyEmpty = 1, --should be in deployable, but who knows...

	Projectile1 = "effects/shotup_mbt.png",
	Projectile2 = "effects/shotup_pyrotank.png",

	Pawn1 = "truelch_MBT",
	Pawn2 = "truelch_PyroTank",

	TipImage = {
		Unit = Point(1,3),
		Target = Point(1,1),
		Enemy = Point(3,1),
		Second_Origin = Point(1,1),
		Second_Target = Point(2,1),
	},
}

truelch_kk_DeployTank_A = truelch_kk_DeployTank:new{
	UpgradeDescription = "Increases the damage dealt by the MBT by 1.\nWhen both upgraded, the MBT become a Pyro Tank.",

	Pawn1 = "truelch_MBT_A",
	Pawn2 = "truelch_MBT_A",
}

truelch_kk_DeployTank_B = truelch_kk_DeployTank:new{
	UpgradeDescription = "Increases the of the MBT by 2.\nWhen both upgraded, the MBT become a Pyro Tank.",

	Pawn1 = "truelch_MBT_B",
	Pawn2 = "truelch_MBT_B",
}

truelch_kk_DeployTank_AB = truelch_kk_DeployTank:new{
	Name = "Deploy Pyro Tank",
	Description = "Deploys a Pyro Tank.",
	Icon = "weapons/truelch_kk_Quartermaster.png", -- notice how the game starts looking in /img/

	Projectile1 = "effects/shotup_pyrotank.png", --pyro

	--Pawn = "truelch_PyroTank",
	Pawn1 = "truelch_PyroTank",
	Pawn2 = "truelch_MBT_AB",

	TipImage = {
		Unit = Point(1,3),
		Target = Point(1,1),
		Enemy = Point(2,1),
		Second_Origin = Point(1,1),
		Second_Target = Point(2,1),
	},
}

function truelch_kk_DeployTank:GetTargetArea(p1)
	local ret = PointList()
	
	for dir = DIR_START, DIR_END do
		for i = 1, 8 do
			local curr = Point(p1 + DIR_VECTORS[dir] * i)
			if not Board:IsValid(curr) then
				break
			end
			
			if not self.OnlyEmpty or not Board:IsBlocked(curr, PATH_GROUND) then
				ret:push_back(curr)
			end
		end
	end
	
	return ret
end

--Is it at rage 1?
function truelch_kk_DeployTank:IsPointBlank(p1, p2)

	LOG("truelch_kk_DeployTank:IsPointBlank(p1: " .. p1:GetString() .. ", p2: " .. p2:GetString() .. ")")

	--local direction = GetDirection(p2 - p1)
	local dir = GetDirection(p2 - p1)

	LOG("A")
	--LOG("dir: " .. tostring(dir))

	local pointBlank = p1 + DIR_VECTORS[dir]

	LOG("B")
	LOG("pointBlank: " .. pointBlank:GetString())

	local isPointBlank = pointBlank == p2

	LOG("isPointBlank: " .. tostring(isPointBlank))

	--LOG("Is")

	return isPointBlank
end

function truelch_kk_DeployTank:GetSkillEffect(p1, p2)
	local ret = SkillEffect()

	LOG("truelch_kk_DeployTank:GetSkillEffect(p1: " .. p1:GetString() .. ", p2: " .. p2:GetString() .. ")")

	local isPointBlank = truelch_kk_DeployTank:IsPointBlank(p1, p2) --works

	if isPointBlank then
		LOG("Is point blank!")
		local damage = SpaceDamage(p2, 0)
		damage.sPawn = self.Pawn1
		ret:AddArtillery(damage, self.Projectile1)
	else
		LOG("Is NOT point blank!")
		local damage = SpaceDamage(p2, 0)
		damage.sPawn = self.Pawn2
		ret:AddArtillery(damage, self.Projectile2)
	end

	return ret
end

truelch_MBT = Deploy_Tank:new{
	Name = "MBT",
	Image = "mbt",
	Health = 1,
	MoveSpeed = 2,
	SkillList = { "truelch_MBT_Cannon" },
}

truelch_MBT_A = truelch_MBT:new{
	SkillList = { "truelch_MBT_Cannon_A" },
}

truelch_MBT_B = truelch_MBT:new{
	Health = 3,
}

--Doesn't exist because when both upgrade you get the Pyro Tank
truelch_MBT_AB = truelch_MBT:new{
	Health = 3,
}

truelch_MBT_Cannon = TankDefault:new {
	Name = "Cannon",
	Description = "Fires a projectile that pushes and deals 1 damage.",
	Class = "",
	Damage = 1,
	Icon = "weapons/deploy_tank.png",
	Explosion = "",
	Sound = "/general/combat/explode_small",
	Damage = 1,
	Push = 1,
	LaunchSound = "/weapons/modified_cannons",
	ImpactSound = "/impact/generic/explosion",
	TipImage = StandardTips.Ranged,
	ZoneTargeting = ZONE_DIR,

	TipImage = {
		Unit = Point(2,3),
		Enemy = Point(2,1),
		Target = Point(2,1),
		CustomPawn = "truelch_MBT"
	}
}

truelch_MBT_Cannon_A = truelch_MBT_Cannon:new {
	Description = "Fires a projectile that pushes and deals 2 damage.",
	Damage = 2,
}

---Fire Tank
truelch_PyroTank = Deploy_Tank:new{
	Name = "Pyro Tank",
	Image = "pyrotank",
	Health = 1,
	MoveSpeed = 3,
	SkillList = { "truelch_PyroTank_Flamethrower" },
}

truelch_PyroTank_Flamethrower = Skill:new{
	Name = "Flamethrower",
	Description = "Ignites and pushes a target in contact.",
	Class = "",
	Icon = "weapons/prime_flamethrower.png",
	Explosion = "",
	Range = 2, -- Tooltip?
	PathSize = 1,
	Damage = 0,
	Push = 1, --Mostly for tooltip, but you could turn it off for some unknown reason
	PowerCost = 0,
	LaunchSound = "/weapons/flamethrower",
	TipImage = {
		Unit = Point(2,3),
		Enemy = Point(2,2),
		Target = Point(2,2),
		CustomPawn = "truelch_PyroTank"
	},
}

function truelch_PyroTank_Flamethrower:GetTargetArea(point)
	local ret = PointList()
	for i = DIR_START, DIR_END do
		for k = 1, self.PathSize do
			local curr = DIR_VECTORS[i]*k + point
			ret:push_back(curr)
			if not Board:IsValid(curr) or Board:GetTerrain(curr) == TERRAIN_MOUNTAIN then
				break
			end
		end
	end
	
	return ret
end
				
function truelch_PyroTank_Flamethrower:GetSkillEffect(p1, p2)
	local ret = SkillEffect()
	local direction = GetDirection(p2 - p1)
	local distance = p1:Manhattan(p2)

	for i = 1, distance do
		local curr = p1 + DIR_VECTORS[direction]*i
		local push = (i == distance) and direction*self.Push or DIR_NONE
		local damage = SpaceDamage(p1 + DIR_VECTORS[direction]*i,0, push)
		damage.iFire = EFFECT_CREATE
		if i == distance then 	
			damage.sAnimation = "flamethrower"..distance.."_"..direction 
		end
		ret:AddDamage(damage)
	end

	return ret
end